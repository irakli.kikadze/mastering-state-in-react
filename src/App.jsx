import React, { useState } from "react";
import "./App.css";
import CommunityComponent from "./CommunityComponent/CommunityComponent.tsx";
import JoinProgram from "./JoinProgram/JoinProgram.tsx";

function App() {
  const [isCommunityComponentVisible, setCommunityComponentVisibility] =
    useState(true);

  const toggleCommunityComponent = () => {
    setCommunityComponentVisibility(!isCommunityComponentVisible);
  };

  return (
    <div className="App">
      <button onClick={toggleCommunityComponent} className="toggle-button">
        {isCommunityComponentVisible ? "Hide Community" : "Show Community"}
      </button>
      {isCommunityComponentVisible && <CommunityComponent />}
      <JoinProgram />
    </div>
  );
}

export default App;
