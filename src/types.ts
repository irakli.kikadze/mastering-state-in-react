export interface CommunityMember {
  id: any;
  firstName: string;
  position: string;
  avatar: string;
}

export interface SubscriptionError {
  error: string;
}

export interface ApiResponse<T> {
  data: T;
}

export interface SubscribeRequest {
  email: string;
}

export interface UnsubscribeRequest {
  email: string;
}
