import React, { useState } from "react";
import { subscribeUser, unsubscribeUser } from "./../api";
import {
  SubscriptionError,
  SubscribeRequest,
  UnsubscribeRequest,
} from "./../types";
import "./index.css";

function JoinProgram() {
  const [email, setEmail] = useState("");
  const [isSubscribing, setIsSubscribing] = useState(false);
  const [isUnsubscribing, setIsUnsubscribing] = useState(false);

  const handleSubscribe = async () => {
    setIsSubscribing(true);

    // Client-side email validation
    const emailPattern = /[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}/;
    if (!emailPattern.test(email)) {
      alert("Please enter a valid email address.");
      setIsSubscribing(false);
      return;
    }

    const request: SubscribeRequest = { email };

    try {
      await subscribeUser(request);
      alert("Subscribed successfully!");
    } catch (error) {
      if (
        error.status === 422 &&
        error.data.error === "Email is already in use"
      ) {
        window.alert("Email is already in use");
      } else {
        console.error("Error subscribing:", error);
      }
    } finally {
      setIsSubscribing(false);
    }
  };

  const handleUnsubscribe = async () => {
    setIsUnsubscribing(true);

    const request: UnsubscribeRequest = { email };

    try {
      await unsubscribeUser(request);
      alert("Unsubscribed successfully!");
    } catch (error) {
      console.error("Error unsubscribing:", error);
    } finally {
      setIsUnsubscribing(false);
    }
  };

  return (
    <div className="join-program">
      <h2>Join Our Program</h2>
      <form>
        <input
          type="email"
          placeholder="Enter your email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}"
          title="Please enter a valid email address"
          className="email"
        />
        <button
          type="button"
          onClick={handleSubscribe}
          disabled={isSubscribing || isUnsubscribing}
          style={{ opacity: isSubscribing || isUnsubscribing ? 0.5 : 1 }}
          className="subscribe"
        >
          Subscribe
        </button>
        <button
          type="button"
          onClick={handleUnsubscribe}
          disabled={isSubscribing || isUnsubscribing}
          style={{ opacity: isSubscribing || isUnsubscribing ? 0.5 : 1 }}
          className="subscribe"
        >
          Unsubscribe
        </button>
      </form>
    </div>
  );
}

export default JoinProgram;
