const API_BASE_URL = "http://localhost:3000";

export async function fetchCommunityData() {
  const response = await fetch(`${API_BASE_URL}/community`);
  if (!response.ok) {
    throw new Error("Failed to fetch community data");
  }
  return response.json();
}

export async function subscribeUser(email) {
  const response = await fetch(`${API_BASE_URL}/subscribe`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(email),
  });

  if (!response.ok) {
    const errorData = await response.json();
    throw { status: response.status, data: errorData };
  }
}

export async function unsubscribeUser(email) {
  const response = await fetch(`${API_BASE_URL}/unsubscribe`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ email }),
  });

  if (!response.ok) {
    throw new Error("Failed to unsubscribe");
  }
}
