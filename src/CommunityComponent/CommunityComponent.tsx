import React, { useEffect, useState } from "react";
import { fetchCommunityData } from "./../api"; // Import your API function and CommunityMember type.
import { CommunityMember } from "./../types";
import "./index.css";
function CommunityComponent() {
  const [communityData, setCommunityData] = useState<CommunityMember[]>([]);

  useEffect(() => {
    fetchCommunityData()
      .then((data) => setCommunityData(data))
      .catch((error) => console.error("Error fetching community data:", error));
  }, []);

  return (
    <section className="user-section">
      <h2>Big Community of People Like You</h2>
      <div className="users">
        {communityData.map((user) => (
          <div key={user.id} className="user">
            <img src={user.avatar} alt={user.firstName} />
            <p className="user-about">{user.position}</p>
            <p className="user-name">{user.firstName}</p>
          </div>
        ))}
      </div>
    </section>
  );
}

export default CommunityComponent;
